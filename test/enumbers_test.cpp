/*
 * MIT License
 * 
 * Copyright (c) 2017 Mattias Sjösvärd (mr@seasword.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *  
 */
#include <enumbers.h>
#include <limits>
#include <cstdint>
#include <iostream>
#include <assert.h>

using namespace com::seasword::enums;
using namespace com::seasword::enums::arithmetic::binary;
using namespace com::seasword::enums::arithmetic::modifiers;
using namespace com::seasword::enums::logical::binary;
using namespace com::seasword::enums::logical::modifiers;

template <typename base_type> 
struct BaseTestClass {

    using Base   = base_type;
    using Limits = std::numeric_limits<Base>;
    
    enum struct Enum : Base {};
    
    static constexpr Enum min       = to_enum_type<Enum>(Limits::min());
    static constexpr Enum nextToMin = to_enum_type<Enum>(Limits::min()+1);
    static constexpr Enum zero      = to_enum_type<Enum>(0);
    static constexpr Enum one       = to_enum_type<Enum>(1);
    static constexpr Enum two       = to_enum_type<Enum>(2);
    static constexpr Enum halfway   = to_enum_type<Enum>((Limits::max()-1)/2);
    static constexpr Enum nextToMax = to_enum_type<Enum>(Limits::max() - 1);
    static constexpr Enum max       = to_enum_type<Enum>(Limits::max());
    
    // Check reversability---

    static_assert( to_underlying_type(min)       == Limits::min(),         "");
    static_assert( to_underlying_type(nextToMin) == (Limits::min() + 1),   "");
    static_assert( to_underlying_type(zero)      == 0,                     "");
    static_assert( to_underlying_type(one)       == 1,                     "");
    static_assert( to_underlying_type(halfway)   == (Limits::max()/2),     "");
    static_assert( to_underlying_type(nextToMax) == (Limits::max() - 1),   "");
    static_assert( to_underlying_type(max)       == Limits::max(),         "");

    // Check basic constexpr properties...
    //
    static_assert( nextToMin   == (min + one),             "");
    static_assert( one         == (zero + one),            "");
    static_assert( (one + one) == to_enum_type<Enum>(2),   "");
    static_assert( max         == (nextToMax + one),       "");
    static_assert( (nextToMax + one) == (one + nextToMax), "");
    static_assert(  Limits::is_signed || ((max + one) == zero), "");
    static_assert( !Limits::is_signed || ((to_enum_type<Enum>(-1) + one) == zero), "");


    static_assert( min       == (nextToMin - one), "");
    static_assert( zero      == (one - one),       "");
    static_assert( one       == (one - zero),      "");
    static_assert( nextToMax == (max - one),       "");
    static_assert(  Limits::is_signed || ((zero - one) == max), "");
    static_assert( !Limits::is_signed || ((zero - one) == to_enum_type<Enum>(-1)), "");

    static_assert( zero == (one * zero), "");
    static_assert( one  == (one * one),  "");
    static_assert( to_enum_type<Enum>(7) * to_enum_type<Enum>(8) == to_enum_type<Enum>(56), "");
    static_assert( (two * halfway + one) == max, "");

    static_assert( zero == (one / two),        "");
    static_assert( one  == (two/two),          "");
    static_assert( one  == (max/max),          "");
    static_assert( (max-one)/two == halfway,   "");
    static_assert( to_enum_type<Enum>(56) / to_enum_type<Enum>(7) == to_enum_type<Enum>(8), "");

    static int runTimeTests() {
        {
            Enum e = zero;
            
            assert( (e += one) == one );
            assert( (e += one) == two );
            
            e = nextToMax;
            
            assert( (e += one) == max );
            assert( Limits::is_signed || ((e += one) == zero) );
            
            e = min;
            
            assert( (e += one) == nextToMin );
        }
        {
            Enum e = two;
            
            assert( (e -= one) == one );
            assert( (e -= one) == zero );
            
            e = max;
            
            assert( (e -= one) == nextToMax );
            
            e = nextToMin;
            
            assert( (e -= one) == min );
            assert( Limits::is_signed || ((e -= one) == max) );
        }
        {
            Enum e = one;
            
            assert( (e *= one) == one );
            assert( (e *= two) == two );
            assert( (e *= two) == two*two );
            
            e = halfway;
            
            assert( (e *= two) == nextToMax );
        }
        {
            Enum e = two*two;
            
            assert( (e /= two) == two );
            assert( (e /= one) == two );
            assert( (e /= two) == one );
            
            e = max;
            
            assert( (e /= two) == halfway );
        }
        {
            Enum e = zero;
            
            assert( ++e == one );
            assert( ++e == two );
            assert( --e == one );
            assert( --e == zero );
            
            if (!Limits::is_signed) {
                assert( --e == max );
                assert( ++e == zero );
            }
            
            assert( e++ == zero );
            assert( e++ == one );
            assert( e-- == two );
            assert( e-- == one );
            
            if (!Limits::is_signed) {
                assert( e-- == zero );
                assert( e++ == max );
            }
        }
        return 0;
    }
    
};

template <typename base_type>
struct UnsignedTestClass : public BaseTestClass<base_type> {
    using typename BaseTestClass<base_type>::Enum;

    using Limits = std::numeric_limits<base_type>;
    
    using BaseTestClass<base_type>::min;
    using BaseTestClass<base_type>::nextToMin;
    using BaseTestClass<base_type>::zero;
    using BaseTestClass<base_type>::one;
    using BaseTestClass<base_type>::two;
    using BaseTestClass<base_type>::halfway;
    using BaseTestClass<base_type>::nextToMax;
    using BaseTestClass<base_type>::max;

    static_assert( zero == (two % two ), "");
    static_assert( zero == (one % one ), "");
    static_assert( max % halfway == one, "");
    
    static_assert( (zero & zero) == zero, "" );
    static_assert( (zero & one ) == zero, "" );
    static_assert( (one  & zero) == zero, "" );
    static_assert( (one  & one ) == one,  "" );
    static_assert( (zero & zero) == zero, "" );
    static_assert( (zero & two ) == zero, "" );
    static_assert( (two  & zero) == zero, "" );
    static_assert( (two  & two ) == two,  "" );
    static_assert( (max  & zero) == zero, "");
    static_assert( (max  & max ) == max,  "");
    
    static_assert( (zero | zero) == zero, "" );
    static_assert( (zero | one ) == one,  "" );
    static_assert( (one  | zero) == one,  "" );
    static_assert( (one  | one ) == one,  "" );
    static_assert( (zero | zero) == zero, "" );
    static_assert( (zero | two ) == two,  "" );
    static_assert( (two  | zero) == two,  "" );
    static_assert( (two  | two ) == two,  "" );
    static_assert( (max  | zero) == max,  "");
    static_assert( (max  | max ) == max,  "");
    
    static_assert( (zero ^ zero) == zero, "" );
    static_assert( (zero ^ one ) == one,  "" );
    static_assert( (one  ^ zero) == one,  "" );
    static_assert( (one  ^ one ) == zero,  "" );
    static_assert( (zero ^ zero) == zero, "" );
    static_assert( (zero ^ two ) == two,  "" );
    static_assert( (two  ^ zero) == two,  "" );
    static_assert( (two  ^ two ) == zero, "" );
    static_assert( (max  ^ zero) == max,  "");
    static_assert( (max  ^ max ) == zero, "");
    
    static_assert( (one >> 1) == zero, "");
    static_assert( (one >> 0) == one, "");
    static_assert( (two >> 1) == one, "");
    static_assert( (max >> (Limits::digits-1)) == one, "");
    static_assert( (max >> 1) == halfway, "");
    
    static_assert( (one  << 1) == two, "");
    static_assert( (one  << 0) == one, "");
    static_assert( (zero << 1) == zero, "");
    static_assert( (max  << 1) == nextToMax, "");
    static_assert( (max << (Limits::digits-1)) == (max ^ (max >> 1)), "");
    
    static_assert( ~zero == max,  "");
    static_assert( ~max  == zero, "");
    static_assert( ~halfway == (one << (Limits::digits-1)), "");
    static_assert( ~one == nextToMax, "");
    
    static int runTimeTests() {
        {
            Enum e = two;
            
            assert( (e %= two) == zero);
            
            e = two + one;
            
            assert( (e %= two) == one );
            
            e = max;
            
            assert( (e %= (two*two)) == (one + two) );
        }

        {
            Enum e = one;
            
            assert( (e &= one) == one  );
            assert( (e &= two) == zero );
            
            e = max;
            
            assert( (e &= halfway) == halfway );
        }
        
        {
            Enum e = zero;
            
            assert( (e |= one) == one  );
            assert( (e |= two) == to_enum_type<Enum>(3) );
        }

        {
            Enum e = one;
            
            assert( (e ^= one) == zero  );
            assert( (e ^= one) == one );
            assert( (e ^= two) == to_enum_type<Enum>(3) );
            assert( (e ^= two) == one );
        }

        {
            Enum e = two;
            
            assert( (e >>= 1) == one );
            
            e = max;
            
            assert( (e >>= 1) == halfway);
            assert( (e >>= (Limits::digits-2)) == one );
        }
        {
            Enum e = one;
            
            assert( (e <<= 1) == two );
            assert( (e <<= 1) == two*two );
            assert( (e <<= (Limits::digits-3)) == (max ^ halfway));
            
        }
        return 0 + BaseTestClass<base_type>::runTimeTests();
    }

};

int main() {
   return BaseTestClass    < std::int8_t   >::runTimeTests() + 
          UnsignedTestClass< std::uint8_t  >::runTimeTests() +
          BaseTestClass    < std::int16_t  >::runTimeTests() + 
          UnsignedTestClass< std::uint16_t >::runTimeTests() +
          BaseTestClass    < std::int32_t  >::runTimeTests() + 
          UnsignedTestClass< std::uint32_t >::runTimeTests() +
          BaseTestClass    < std::int64_t  >::runTimeTests() + 
          UnsignedTestClass< std::uint64_t >::runTimeTests();
           
}
