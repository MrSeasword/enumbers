/*
 * MIT License
 * 
 * Copyright (c) 2017 Mattias Sjösvärd (mr@seasword.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *  
 */

//======================================================================
// This file provides arithmetic and logical operations for enumerated 
// types in C++14.
// The functionality provided herein utilizes the fact that you can 
// create new "integer types" via a scoped enum (preferrably with a 
// specified base type), like this:
//
//    enum struct UnsignedByte : std::uint8_t {};
// 
// This somewhat strange declaration gives us a type of 8 bits size with
// relational and assignment operators. It is also a distinct type since
// it does not have any implicit integer conversions. 
// This is quite useful when you want to use strong typing with the size
// and efficiency of built-in types.
//
// Now why would you want that?
// Imagine e.g. a version tuple consisting of a major and minor version.
// When handling this tuple (be it a class, pair or tuple!), you will 
// need to compare different instances of this tuple and you do NOT want
// to compare major vs minors. By making majors and minors of a different
// "new integer" type, you will catch errors of this kind during build
// time.
//
// The only drawback is that these types does not come with arithmetic or
// logical operations, which limits their use as integer types.
// Enter enumbers.h!
// This header provides the usual operations +,-,*,/,&,|,~,>> and << as
// well as their assignment variants and pre- and postfix operations.
//
//

#ifndef COM_SEASWORD_ENUMBERS_H
#define COM_SEASWORD_ENUMBERS_H

#include <type_traits>

namespace com { namespace seasword { namespace enums { 

  //====================================================================
  // The following four functions are cast functions with the extra
  // requirement that the type involved is an enum type. 
  // The to_enum_type could of course be used as a constructor function.
  //

  template <typename enum_type>
  inline constexpr std::underlying_type_t<enum_type> 
  to_underlying_type( enum_type v ) noexcept {
    return static_cast<std::underlying_type_t<enum_type>>(v);
  }

  template <typename enum_type>
  inline constexpr std::underlying_type_t<enum_type> &
  to_underlying_type_ref( enum_type &v ) noexcept {
    return reinterpret_cast<std::underlying_type_t<enum_type>&>(v);
  }

  template <typename enum_type>
  inline constexpr enum_type
  to_enum_type( std::underlying_type_t<enum_type> v ) noexcept {
    return static_cast<enum_type>(v);
  }

  template <typename enum_type>
  inline constexpr enum_type &
  to_enum_type_ref( std::underlying_type_t<enum_type> &v ) noexcept {
    return reinterpret_cast< enum_type &>(v);
  }

  namespace arithmetic {

    //==================================================================
    // The arithmetic functions are provided for both unsigned and 
    // signed types, except for remainder which is only provided for
    // unsigned.
    // The binary operators are constexpr, but this is not the case for
    // the modifying operators for obvious reasons.

    namespace binary {

      template <typename enum_type>
      inline constexpr std::enable_if_t<std::is_enum<enum_type>::value,enum_type> 
      operator + ( enum_type lhs, enum_type rhs) noexcept {
	return to_enum_type<enum_type>( to_underlying_type(lhs) + to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline constexpr std::enable_if_t<std::is_enum<enum_type>::value,enum_type> 
      operator - ( enum_type lhs, enum_type rhs) noexcept {
	return to_enum_type<enum_type>( to_underlying_type(lhs) - to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline constexpr std::enable_if_t<std::is_enum<enum_type>::value,enum_type> 
      operator * ( enum_type lhs, enum_type rhs) noexcept {
	return to_enum_type<enum_type>( to_underlying_type(lhs) * to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline constexpr std::enable_if_t<std::is_enum<enum_type>::value,enum_type> 
      operator / ( enum_type lhs, enum_type rhs) noexcept {
	return to_enum_type<enum_type>( to_underlying_type(lhs) / to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline constexpr std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type> 
      operator % ( enum_type lhs, enum_type rhs) noexcept {
	return to_enum_type<enum_type>( to_underlying_type(lhs) % to_underlying_type(rhs) );
      }

    }

    namespace modifiers {

      template <typename enum_type>
      inline std::enable_if_t<std::is_enum<enum_type>::value,enum_type>& 
      operator ++ ( enum_type &value ) noexcept {
	return to_enum_type_ref<enum_type>( ++to_underlying_type_ref(value) );
      }
    
      template <typename enum_type>
      inline std::enable_if_t<std::is_enum<enum_type>::value,enum_type>
      operator ++ ( enum_type &value, int ) noexcept {
	return to_enum_type<enum_type>( to_underlying_type_ref(value)++ );
      }

      template <typename enum_type>
      inline std::enable_if_t<std::is_enum<enum_type>::value,enum_type>& 
      operator -- ( enum_type &value ) noexcept {
	return to_enum_type_ref<enum_type>( --to_underlying_type_ref(value) );
      }
    
      template <typename enum_type>
      inline std::enable_if_t<std::is_enum<enum_type>::value,enum_type>
      operator -- ( enum_type &value, int ) noexcept {
	return to_enum_type<enum_type>( to_underlying_type_ref(value)-- );
      }

      template <typename enum_type>
      inline  std::enable_if_t<std::is_enum<enum_type>::value,enum_type>&
      operator += ( enum_type &lhs, enum_type rhs) noexcept {
	return to_enum_type_ref<enum_type>( to_underlying_type_ref(lhs) += to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline  std::enable_if_t<std::is_enum<enum_type>::value,enum_type>& 
      operator -= ( enum_type &lhs, enum_type rhs) noexcept {
	return to_enum_type_ref<enum_type>( to_underlying_type_ref(lhs) -= to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline  std::enable_if_t<std::is_enum<enum_type>::value,enum_type>&
      operator *= ( enum_type &lhs, enum_type rhs) noexcept {
	return to_enum_type_ref<enum_type>( to_underlying_type_ref(lhs) *= to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline  std::enable_if_t<std::is_enum<enum_type>::value,enum_type>&
      operator /= ( enum_type &lhs, enum_type rhs) noexcept {
	return to_enum_type_ref<enum_type>( to_underlying_type_ref(lhs) /= to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline constexpr std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type> 
      operator %= ( enum_type lhs, enum_type rhs) noexcept {
	return to_enum_type_ref<enum_type>( to_underlying_type_ref(lhs) %= to_underlying_type(rhs) );
      }
    }
  }

  namespace logical {

    //==================================================================
    // The logical functions are provided for unsigned types since it is
    // dubious to perfom logic on signed numbers.
    // 
    // The binary operators are constexpr, but this is not the case for
    // the modifying operators for obvious reasons.

    namespace binary {
      template <typename enum_type>
      inline constexpr std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type> 
      operator & ( enum_type lhs, enum_type rhs) noexcept {
	return to_enum_type<enum_type>( to_underlying_type(lhs) & to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline constexpr std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type> 
      operator | ( enum_type lhs, enum_type rhs) noexcept {
	return to_enum_type<enum_type>( to_underlying_type(lhs) | to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline constexpr std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type> 
      operator ^ ( enum_type lhs, enum_type rhs) noexcept {
	return to_enum_type<enum_type>( to_underlying_type(lhs) ^ to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline constexpr std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type> 
      operator << ( enum_type lhs, std::size_t amount ) noexcept {
	return to_enum_type<enum_type>( to_underlying_type(lhs) << amount );
      }

      template <typename enum_type>
      inline constexpr std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type> 
      operator >> ( enum_type lhs, std::size_t amount ) noexcept {
	return to_enum_type<enum_type>( to_underlying_type(lhs) >> amount );
      }

      template <typename enum_type>
      inline constexpr std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type> 
      operator ~ ( enum_type v ) noexcept {
	return to_enum_type<enum_type>( ~ to_underlying_type(v) );
      }
    }

    namespace modifiers {

      template <typename enum_type>
      inline  std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type>&
      operator &= ( enum_type &lhs, enum_type rhs) noexcept {
	return to_enum_type_ref<enum_type>( to_underlying_type_ref(lhs) &= to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline  std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type>& 
      operator |= ( enum_type &lhs, enum_type rhs) noexcept {
	return to_enum_type_ref<enum_type>( to_underlying_type_ref(lhs) |= to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline  std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type>&
      operator ^= ( enum_type &lhs, enum_type rhs) noexcept {
	return to_enum_type_ref<enum_type>( to_underlying_type_ref(lhs) ^= to_underlying_type(rhs) );
      }

      template <typename enum_type>
      inline  std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type>& 
      operator >>= ( enum_type &lhs, std::size_t amount ) noexcept {
	return to_enum_type_ref<enum_type>( to_underlying_type_ref(lhs) >>= amount );
      }

      template <typename enum_type>
      inline  std::enable_if_t<std::is_unsigned<std::underlying_type_t<enum_type>>::value,enum_type>& 
      operator <<= ( enum_type &lhs, std::size_t amount ) noexcept {
	return to_enum_type_ref<enum_type>( to_underlying_type_ref(lhs) <<= amount );
      }

    }
  }

}}}

#endif
