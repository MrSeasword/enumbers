# README #

### What is this repository for? ###

This repository contains a header-only solution for performing arithmetic and logical operations on C++14 enumerated types.

### How do I get set up? ###

Clone this repo and include the file enumbers.h into your source where you need the operations provided.

There is no configuration or build needed since this is a header-only solution.

To run unit tests run cmake, make and then make the "test" target.

Deployment is up to you. Install the header file enumbers.h at a place of your own choice.

### Contribution guidelines ###

Contributions are welcome.

### Who do I talk to? ###

mr@seasword.com
